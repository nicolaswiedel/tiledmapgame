package de.tiledmapgame;

/**
 * Created by Nicolas Wiedel on 26.03.2019.
 */
public class GameConfig {

    public static final float WIDTH = 960f;
    public static final float HEIGHT = 640f;

    public static final float WORLD_WIDTH = 30;
    public static final float WORLD_HEIGHT = 20;

    private GameConfig(){

    }
}
