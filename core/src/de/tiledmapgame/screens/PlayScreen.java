package de.tiledmapgame.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import de.tiledmapgame.GameConfig;
import de.tiledmapgame.GdxUtils;
import de.tiledmapgame.TiledMapGame;
import de.tiledmapgame.entities.Player;

/**
 * Created by Nicolas Wiedel on 26.03.2019.
 */
public class PlayScreen implements Screen {

    private final TiledMapGame game;
    private OrthogonalTiledMapRenderer renderer;
    private TmxMapLoader loader = new TmxMapLoader();
    private OrthographicCamera camera;

    private TiledMap map;

    private Player player;

    public PlayScreen(TiledMapGame game){
        this.game = game;
    }

    @Override
    public void show() {
        map = loader.load("maps/map.tmx");

        renderer = new OrthogonalTiledMapRenderer(map);
        camera = new OrthographicCamera();

       player = new Player(new Sprite(new Texture("badlogic.jpg")));
    }

    @Override
    public void render(float delta) {
        GdxUtils.clearScreen();

        camera.position.set(GameConfig.WIDTH / 2, GameConfig.HEIGHT / 2, 0);
        camera.update();

        renderer.setView(camera);
        renderer.render();

        game.getBatch().begin();

        player.draw(game.getBatch());

        game.getBatch().end();
    }

    @Override
    public void resize(int width, int height) {
        camera.viewportWidth = width;
        camera.viewportHeight = height;
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        map.dispose();
        renderer.dispose();
        player.getTexture().dispose();
    }
}
