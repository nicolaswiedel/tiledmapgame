package de.tiledmapgame.entities;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import de.tiledmapgame.screens.PlayScreen;

/**
 * Created by Nicolas Wiedel on 27.03.2019.
 */
public class Player extends Sprite {

    private Vector2 velocity = new Vector2();

    private float speed = 60f * 2f;
    private float gravity = 60f * 1.8f;

    public Player(Sprite sprite){
        super(sprite);
    }

    @Override
    public void draw(Batch batch) {
        update(Gdx.graphics.getDeltaTime());
        super.draw(batch);
    }

    public void update(float delta){

        velocity.y -= gravity *delta;

        if(velocity.y > speed){
            velocity.y = speed;
        }
        else if(velocity.y < -speed){
            velocity.y = -speed;
        }

        setX(getX() + velocity.x * delta);
        setY(getY() + velocity.y * delta);
    }
}
