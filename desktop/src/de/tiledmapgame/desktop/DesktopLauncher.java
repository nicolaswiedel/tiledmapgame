package de.tiledmapgame.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

import de.tiledmapgame.GameConfig;
import de.tiledmapgame.TiledMapGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();

		config.title = "TiledMapGame";
		config.width = (int) GameConfig.WIDTH;
		config.height = (int)GameConfig.HEIGHT;

		new LwjglApplication(new TiledMapGame(), config);
	}
}
